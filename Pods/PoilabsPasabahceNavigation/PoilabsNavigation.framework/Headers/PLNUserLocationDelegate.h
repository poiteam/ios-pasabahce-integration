//
//  PLNUserLocationDelegate.h
//  PoilabsNavigation
//
//  Created by Emre Kuru on 29.08.2020.
//  Copyright © 2020 poilabs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PLPoi.h"
#import <CoreLocation/CoreLocation.h>

@protocol PLNUserLocationDelegate <NSObject>

@optional

+(instancetype)sharedInstance;

-(void)userLocation:(NSString *)physicalId;

@end
