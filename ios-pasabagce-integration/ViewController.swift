//
//  ViewController.swift
//  ios-pasabagce-integration
//
//  Created by Emre Kuru on 15.09.2020.
//  Copyright © 2020 Emre Kuru. All rights reserved.
//

import UIKit
import PoilabsNavigation

class ViewController: UIViewController {

    
    @IBOutlet weak var navigationView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadMap()
        // Do any additional setup after loading the view.
    }

    var currentCarrier: PLNNavigationMapView?

    func loadMap() {
        PLNNavigationSettings.sharedInstance().applicationId = "bdeac7eb-f2af-4d4c-be03-4f71cca357d1"
        PLNNavigationSettings.sharedInstance().applicationSecret = "f1411dac-f7d7-459e-a078-769eca4cb631"
        PLNNavigationSettings.sharedInstance()?.navigationUniqueIdentifier = "UNIQUE ID"

        //aplication language tr/en
        PLNNavigationSettings.sharedInstance()?.applicationLanguage = "tr"

        //Haritada gösterilecek pointlerin set edilmesi
        var storeIds = [String]()
        storeIds.append("storeId1")
        storeIds.append("storeId2")
        //PLNNavigationSettings.sharedInstance()?.storeIds = storeIds
        
        var starIds = [String]()
        starIds.append("2")
        starIds.append("20")
        starIds.append("22")
        PLNNavigationSettings.sharedInstance()?.starPoints = starIds
        //show loading animation
        PLNavigationManager.sharedInstance()?.getReadyForStoreMap(completionHandler: { (error) in

        //hide loading animation
                if error == nil {
                    let carrierView = PLNNavigationMapView(frame: CGRect(x: 0, y: 0, width: self.navigationView.bounds.size.width, height: self.navigationView.bounds.size.height))
                    carrierView.awakeFromNib()
                    carrierView.delegate = self as? PLNNavigationMapViewDelegate
                    self.currentCarrier = carrierView
                    self.navigationView.addSubview(carrierView)

                    //userLocationDelegate'in set edilmesi
                    PLNLocationManager.sharedInstance()?.userLocationDelegate = self
            }
        })
    }
    
    @IBAction func getRoute(_ sender: Any) {
        PLNNavigationSettings.sharedInstance()?.getRoute("20")
    }
    
    @IBAction func showOnMap(_ sender: Any) {
        self.currentCarrier?.getShowonMapPin("20")
    }
    
}

extension ViewController: PLNUserLocationDelegate {
    func userLocation(_ physicalId: String!) {
        print(physicalId)
    }
}
